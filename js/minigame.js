$(document).ready(function(){
    var inProcess_play = false;
    $("#do").click(function(){
        if (!inProcess_play) {
            inProcess_play = true;
            $.ajax({
                type: "POST",
                url: "xuly.php",
                cache: false,
                dataType: 'json',
                data: {
                    play : $('#do').val()
                },
                success: function(res){
                    inProcess_play = false;

                    $("#img1").attr("src","img/iconquayso/hinh-" + res.data.a + ".png");
                    $("#img2").attr("src","img/iconquayso/hinh-" + res.data.b + ".png");
                    $("#img3").attr("src","img/iconquayso/hinh-" + res.data.c + ".png");

                    if (res.status == false) {
                        $("#do").attr("disabled","disabled");
                    }

                    $('#dial .modal-body').html(res.message);
                    $('#dial').modal('show');
                }
            });
        }
        return false;
    });

    $("#btnRegister").click(function(){
        if (!inProcess_play) {
            inProcess_play = true;
            var frmData = $('#frmRegister').serialize();
            $.ajax({
                type: "POST",
                url: "exportexcel.php",
                cache: false,
                dataType: 'json',
                data: frmData,
                success: function(res){
                    inProcess_play = false;
                    if (res.status == true) {
                        $("#frmRegister").find('input, textarea').attr('disabled','disabled');
                        $("#btnRegister").hide();
                        $("#do").show();
                    }

                    $('#dial .modal-body').html(res.message);
                    $('#dial').modal('show');
                }
            });
        }
        return false;
    });
});