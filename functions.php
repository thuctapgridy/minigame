<?php
/**
 * functions.php
 * Include functions use for project
 */

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function checkExisted ($ip = '') {
	if ( !empty($ip) ) {
		$__data = json_decode(file_get_contents('guest.json'), true);
		
		if ($__data) {
			foreach ($__data as $guest) {
		        if ($guest['ip'] == $ip) return true;
		    }
		    return false;
		}
	    return false;
	}
	return true;
}

function getNextClientIndex () {
	$__data = json_decode(file_get_contents('guest.json'), true);
	if ( !empty($__data) ) {
		return count($__data);
	}
	return 0;
}

function getInfoClient ($ip = '') {
	if ( !empty($ip) ) {
		$handle = fopen("clientdata.csv", "r");
		if ( $handle ) {
		    while ( ( $line = fgets($handle) ) !== false ) {
		        $data = explode(',', $line);
		        if ( $data[0] == $ip ) {
		        	return [
		        		'fullname' 	=> $data[1],
		        		'phone' 	=> $data[2],
		        		'email' 	=> $data[3],
		        		'company' 	=> $data[4]
		        	];
		        }
		    }
		    fclose($handle);
		}
		return null;
	}
	return null;
}

function checkInfoClient ($email = '', $phone = '') {
	if ( empty($email) && empty($phone) ) {
		return false;
	} else {
		$handle = fopen("clientdata.csv", "r");
		if ( $handle ) {
		    while ( ( $line = fgets($handle) ) !== false ) {
		        $data = explode(',', $line);
		        if ( $data[2] == $phone || $data[3] == $email ) {
		        	return true;
		        }
		    }
		    return false;
		}
		return null;
	}
}

function addNewClient ($ip = '', $num = 3) {
	if ( !empty($ip) ) {
		$__data = json_decode(file_get_contents('guest.json'), true);

		$__data[] = [
            'index' => getNextClientIndex(),
            'ip' => $ip,
            'num' => $num
        ];

        file_put_contents('guest.json', json_encode($__data));

        return true;
	}

	return false;
}