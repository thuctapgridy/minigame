<?php

include 'functions.php';
// Get info client to check existed in database
$client_ip = get_client_ip();
$client_info = [
    'fullname'  => '',
    'email'     => '',
    'phone'     => '',
    'company'   => ''
];
$disabled = '';
if ( checkExisted($client_ip) ) {
    $getCLInfo = getInfoClient($client_ip);
    if ($getCLInfo) {
        $client_info = $getCLInfo;
        $disabled = ' disabled';
    }
}

?>
<!DOCTYPE html>
<html lang="vi">
<head>
	<title>Đất Hợp</title>
	<meta charset="UTF-8">
	<meta name="description" content="Labs - Design Studio">
	<meta name="keywords" content="lab, onepage, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="http://dathop.com.vn/giaodien/default/images/logo.png" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,700|Roboto:300,400,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/flaticon.css"/>
	<link rel="stylesheet" href="css/magnific-popup.css"/>
	<link rel="stylesheet" href="css/owl.carousel.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/media.css"/>
    <script src="js/jquery-2.1.4.min.js"></script>


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader">
			<img src="http://dathop.com.vn/giaodien/default/images/logo.png" alt="">
			<h2>Loading.....</h2>
		</div>
	</div>


	<!-- Header section -->
	<header class="header-section">
        <div class="container">
            <div class="logo">
                <img src="img/iconquayso/hinh-1.png" alt=""><!-- Logo -->
            </div>
            <!-- Navigation -->
            <div class="responsive"><i class="fa fa-bars"></i></div>
            <nav>
                <ul class="menu-list">
                    <li class="active"><a href="http://dathop.com.vn/post/gioi-thieu-tong-quan.html">Giới Thiệu</a></li>
                    <li><a href="http://dathop.com.vn/tonghop.php">Sản Phẩm</a></li>
                    <li><a href="http://dathop.com.vn/moitruong.php">Môi Trường</a></li>
                    <li><a href="http://dathop.com.vn/cate/tin-tuc.html">Tin Tức</a></li>
                    <li><a href="http://dathop.com.vn/cate/tuyen-dung.html">Tuyển Dụng</a></li>
                    <li><a href="http://dathop.com.vn/lienhe.php">Liên Hệ</a></li>

                </ul>
            </nav>
        </div>
	</header>
	<!-- Header section end -->


	<!-- Intro Section -->
<!--	<div class="hero-section">-->
<!--		<div class="hero-content">-->
<!--			<div class="hero-center">-->
<!---->
<!--			</div>-->
<!--		</div>-->
<!--		<!-- slider -->-->
<!--		<div id="hero-slider" class="owl-carousel">-->
<!--			<div class="item  hero-item" data-bg="img/dathop/slide1.png"></div>-->
<!--			<!--<div class="item  hero-item" data-bg="img/02.jpg"></div>-->-->
<!--		</div>-->
<!--	</div>-->
    <div class="background-header">
        <img src="img/background/BACKGROUND1.png">
        <button class="click-gift" href="#gift"></button>
<!---->
<!--        <div class="balloon">-->
<!--            <div><span>☺</span>-->
<!--            </div>-->
<!--            <div><span>B</span>-->
<!--            </div>-->
<!--            <div><span>D</span>-->
<!--            </div>-->
<!--            <div><span>A</span>-->
<!--            </div>-->
<!--            <div><span>Y</span>-->
<!--            </div>-->
<!--            <div><span>☺</span>-->
<!--            </div>-->
<!--        </div>-->
    </div>
	<!-- Intro Section -->

    <div class="how-to-play">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <a href="http://dathop.com.vn/post/sinh-nhat-choi-lon-click-ngay-co-qua-mung-sinh-nhat-15-nam-dat-hop.html"><h1>Hướng dẫn săn quà</h1></a>
                    <form class="form-class" id="frmRegister">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <input type="text" name="fullname" placeholder="Họ Tên" value="<?php echo $client_info['fullname']; ?>"<?php echo $disabled; ?> required>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <input type="text" name="email" placeholder="Email" value="<?php echo $client_info['email']; ?>"<?php echo $disabled; ?> required>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <input type="text" name="phone" placeholder="Số Điện Thoại" value="<?php echo $client_info['phone']; ?>"<?php echo $disabled; ?> required>
                                <textarea name="company" placeholder="Công Ty"<?php echo $disabled; ?> required><?php echo $client_info['company']; ?></textarea>
                                
                            </div>
                            <div class="col-sm-12 col-xs-12 dial">
                                <?php if (empty($disabled)): ?>
                                    <button class="site-btn" id="btnRegister">Đăng Ký</button>
                                <?php endif ?>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 contact-info">
                        <div class="wrapper text-center main-part" align="center">
                                <div class="section-body-inner section-body-inner-primary">
                                    <ul class="slots " id="slots">
                                        <li><img id="img1" src="img/iconquayso/hinh-0.png"
                                                 alt="Hình A" style='height: 90%; width: 90%; object-fit: contain'></li>
                                        <li><img id="img2" src="img/iconquayso/hinh-0.png"
                                                 alt="Hình B" style='height: 90%; width: 90%; object-fit: contain'></li>
                                        <li><img id="img3" src="img/iconquayso/hinh-0.png"
                                                 alt="Hình C" style='height: 90%; width: 90%; object-fit: contain'></li>
                                    </ul>
                                </div>

                            <form method="post" >
                                <button type="submit" id="do" name="do" value="1" class="btn btn-yellow btn-yellow-secondary action-dial" <?php echo (empty($disabled) ? 'style="display:none;"' : ''); ?>>Quay ngay nhận quà</button>
                            </form>
                    </div>
				</div>
            </div>
        </div>
    </div>
    
    <!-- Popup -->
    <div class="modal fade" id="ready-show" role="dialog">    
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h1>Sinh Nhật Đất Hợp  </h1>
                    <h2>Đăng Ký Nhanh Săn Quà Khủng</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="dial" role="dialog">    
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <!-- End Popup -->
	

	<!-- Footer section -->
	<footer class="footer-section">
		<h2>2017 All rights reserved. Thiết Kế bởi <a href="https://colorlib.com" target="_blank">GridyVN</a></h2>
	</footer>
	<!-- Footer section end -->




	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
    <script src="js/circle-progress.min.js"></script>
	<script src="js/minigame.js"></script>
    <script src="js/main.js"></script>
   <script>
        $(".click-gift").click(function() {
            $('html, body').animate({
                scrollTop: $(".how-to-play").offset().top
            }, 1200);
        });
   </script>
</body>
</html>
