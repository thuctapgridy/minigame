<?php

include 'functions.php';

// Khởi tạo cấu hình chung
$arr_img = [1,2,3,4,5]; // Mảng chứa hình ảnh
$arr_winner = [1,2]; // STT client được giải nhất
$num_allow = 3; // Số lần cho phép quay

$global_config = [
    'img_winner' => 1 // hình ảnh giải nhất kế tiếp
];

// Tạo file config_game.json chứa cấu hình
if (!file_exists('config_game.json')) {
    file_put_contents('config_game.json', json_encode($global_config));
} else {
    $global_config = json_decode(file_get_contents('config_game.json'), true);
}

// Khởi tạo client
$client_ip = get_client_ip();
$client_num = $num_allow;
$client_index = 0;

// Khởi tạo dữ liệu
$__data = [];

// Tạo file guest.json chứa dữ liệu
if (!file_exists('guest.json')) {
    file_put_contents('guest.json', "");
} else {
    $__data = json_decode(file_get_contents('guest.json'), true);
}

// Kiểm tra tồn tại của client trong dữ liệu:
// Chưa có thì tạo mới, có rồi thì lấy số lần đã quay của client
$client_existed = false;
if (empty($__data)) {
    $__data[] = [
        'index' => $client_index,
        'ip' => $client_ip,
        'num' => $client_num
    ];
} else {
    foreach ($__data as $guest) {
        if ($guest['ip'] == $client_ip) {
            $client_index = $guest['index'];
            $client_num = $guest['num'];
            $client_existed = true;
        }
    }
    if (!$client_existed) {
        $__data[] = [
            'index' => count($__data),
            'ip' => $client_ip,
            'num' => $client_num
        ];
    }
}

// Kiểm tra số lần quay còn lại
$allow_play = true;
if ($client_num == 0) {
    $allow_play = false;
}

// Xử lý chính
$result = [];

if (isset($_POST['play'])) {
    if ($allow_play == false) {
        // Không được phép quay nữa
        $result['status'] = false;
        $result['message'] = 'Bạn đã quay đủ số lần';
        $result['data'] = [
            'a' => 0,
            'b' => 0,
            'c' => 0
        ];

    } else {
        // Được phép quay
        $a = $b = $c = 0;

        $a = $arr_img[array_rand($arr_img)];
        if ($a >= count($arr_img)) $a = 1;

        $b = $arr_img[array_rand($arr_img)];
        if ($b >= count($arr_img)) $b = 1;

        $c = $arr_img[array_rand($arr_img)];

        if ($a == $b && $b == $c) $c++;
        if ($c >= count($arr_img)) $c = 1;

        if ($client_num == 1) {
            if ( in_array($client_index, $arr_winner)) {
                // Xử lý giải nhất
                $a = $b = $c = $global_config['img_winner'];
                $global_config['img_winner'] = $global_config['img_winner'] + 1;
                if ($global_config['img_winner'] == count($arr_img)) $global_config['img_winner'] = 1;
                
                // Save info client to csv
                header( "Content-Type: application/csv;charset=utf-8" );
                $client_info = getInfoClient($client_ip);
                $cvsData = $client_ip . "," . $client_info['fullname'] . "," . $client_info['phone'] . "," . $client_info['email'] . "," . trim($client_info['company'])  . ",1," . $a . "\n";
                $fp = fopen("clientdata_win.csv", "a");
                fwrite($fp, $cvsData);
                fclose($fp);
                
                // Show content to popup
                $result['message'] = '<div class="title-prize">Chúc mừng bạn nhận được voucher trị giá 2 triệu VNĐ</div>';
                $result['message'] .= file_get_contents('./win_content/' . $a . '.html', FILE_USE_INCLUDE_PATH);

            } else {

                $result['message'] = '<div class="title-prize">Cảm Ơn Bạn Đã Đồng Hành Cùng Đất Hợp 15 Năm</div>';
                $result['message'] .= file_get_contents('./win_content/0.html', FILE_USE_INCLUDE_PATH);
            }

        } else {
            $result['message'] = '<div class="title-prize">MỜI BẠN QUAY TIẾP!</div>';
        }

        $client_num = $client_num - 1;
        // Xử lý lưu lại thông tin client
        foreach ($__data as $k => $guest) {
            if ($__data[$k]['ip'] == $client_ip) {
                $__data[$k]['num'] = $client_num;
            }
        }
        file_put_contents('guest.json', json_encode($__data));
        
        // Lưu thông tin cấu hình
        file_put_contents('config_game.json', json_encode($global_config));

        $result['status'] = true;
        $result['data'] = [
            'a' => $a,
            'b' => $b,
            'c' => $c
        ];
    }

    echo json_encode($result);
    die;
}