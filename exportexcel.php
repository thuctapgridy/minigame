<?php

include 'functions.php';

$client_ip 	= get_client_ip();

$fullname	= $_POST["fullname"];
$phone		= $_POST["phone"];
$email 		= $_POST["email"];
$company 	= $_POST["company"];

header( "Content-Type: application/csv;charset=utf-8" );

if(!empty($fullname) && !empty($phone) && !empty($company)) {
	if ( checkExisted($client_ip) ) {
		echo json_encode(['status' => false, 'message' => 'Đăng ký không được phép.']);
		die;
	} else {
		if ( checkInfoClient($email, $phone) ) {
			echo json_encode(['status' => false, 'message' => 'Email hoặc Số điện thoại đã được đăng ký rồi, hãy nhập thông tin khác.']);
			die;
		} else {
			if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			  	echo json_encode(['status' => false, 'message' => 'Email không hợp lệ.']);
			  	die;
			} else {
				if( !preg_match("/^[0][0-9]{9,10}$/", $phone) ){
					echo json_encode(['status' => false, 'message' => 'Số điện thoại không hợp lệ.']);
					die;
				} else {
					if ( !addNewClient ($client_ip) ) {
						echo json_encode(['status' => false, 'message' => 'Xảy ra lỗi khi lưu thông tin đăng ký.']);
						die;
					} else {
						$cvsData = $client_ip . "," . $fullname . "," . $phone . "," . $email . "," . $company  . "\n";

					    $fp = fopen("clientdata.csv", "a");

					    if ( $fp ) {
					        fwrite($fp, $cvsData);
					        fclose($fp);
					        echo json_encode(['status' => true, 'message' => 'Đăng ký thông tin thành công.<br>Quay ngay, bạn có thể nhận được voucher trị giá 2 triệu từ chúng tôi.']);
					        die;
					    } else {
					    	echo json_encode(['status' => false, 'message' => 'Xảy ra lỗi khi lưu thông tin đăng ký.']);
					    	die;
					    }
					}
				}
			}
		}
	} 
} else {
	echo json_encode(['status' => false, 'message' => 'Hãy nhập đầy đủ thông tin đăng ký.']);
	die;
}